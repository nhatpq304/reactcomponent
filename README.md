# React Web Component

This project serves as a web component for [Micro Frontend Host](https://bitbucket.org/nhatpq304/microfrontendhost/src/master) project.

## Build

Run `npm run build` to build the project and create a web component script in root directory.

## Start

Uncomment `ReactDOM.render ...` in `index.js`.

Run `npm run start` to run the project.

