import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import First from './First';
import Second from './Second';
import Main from './Main';

function App() {
  return (
    <div className="App">
      REACT
      <BrowserRouter>
        <Routes>
        <Route path={'react'}>
          <Route index element={<Main/>}/>
          <Route path={'first'} element={<First/>}/>
          <Route path={'second'} element={<Second/>}/>
        </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
